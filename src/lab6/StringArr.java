package lab6;

public class StringArr {
	
	private String [] array;
	private int index;
	private final int MAX = 100;
	
	
	//default constructor, instantiates array to MAX size and assigns 0 to index
	public StringArr() {
		this.array = new String [MAX];
		this.index = 0;
	}
		
	//nondefault constructor to instantiate an array to size, assigns 0 to index
	public StringArr(int size) {
		this.array = new String [size];
		this.index = 0;
		
	}
		
	//nondefault constructor that will assign a new array and new value to index
	public StringArr(String [] anArr, int newIndex) {
		this.array = anArr;
		this.index = newIndex;
		
	}

	//return a copy of the string array, accessor
	public String[] getstrArr() {
		String [] copy = this.array;
		return copy;
	}

	//assign a new string array, mutator
	public void setstrArr(String[] aStrArr) {
		this.array = aStrArr;
	}

	//return # of actual data in array, accessor
	public int getIndex() {
		return this.index;
	}

	//assign a new index, mutator
	public void setIndex(int anIndex) throws IndexOutOfBoundsException {
		if(anIndex < 0 || anIndex >= this.array.length)
			throw new IndexOutOfBoundsException();
		else this.index = anIndex;
	}

	//return an object at given pos
	public String getObject(int pos) throws IndexOutOfBoundsException {
		if (pos < 0 || pos >= this.array.length) 
			throw new IndexOutOfBoundsException();
		else return this.array[pos];
	}
		
	//return the string with contents of array
	public String toString() {
		String toString = "";
		for (int i = 0; i < this.array.length; i++) {
			toString = toString + this.array[i] + "\n";
		}
		return toString;
	}
		
	//return true if calling object is equivalent to arg; length, index, and each element should be the equal
	public boolean equals(StringArr anArr) {
		if (this.index == anArr.getIndex()
				&& this.array.length == anArr.getstrArr().length
				&& this.array == anArr.getstrArr()) {
			for (int i = 0; i < this.array.length; i++) {
				if (this.array[i] != anArr.getstrArr()[i])
					return false;
			}
			return true;
		}
		else return false;
	}

	//insert a new string into the array if there is room, inc index
	public void add(String obj) {
		 if(this.index >= this.array.length) {
			 System.out.println("Array full.");
			 return;
		 } else {
			 this.array[this.index] = obj;
			 index++;
		 }
	}

}
