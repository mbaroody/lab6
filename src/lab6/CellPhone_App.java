package lab6;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CellPhone_App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("+-------------------------------+"
				+ "\n+           ADD ONS             +" 
				+ "\n+-------------------------------+");
		System.out.println("\nPlease choose from the following options."
				+ "\nChoose an option only once.\n");
		
		boolean one = false;
		boolean two = false;
		boolean three = false;
		boolean four = false;
		int option = 0;
		Scanner reader = new Scanner(System.in);
		while (option != -1) {
			System.out.println("\n 1. <= 200 text messages/month $5"
					+ "\n 2. additional line $9.99"
					+ "\n 3. international calling $3.99"
					+ "\n 4. early nights and weekends $16.99");
			try {
				option = reader.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Please enter an integer.");
				reader.nextLine();
				continue;
			}
			
			if (option > 4 || option < -1 || option == 0) 
				System.out.println("Please choose a valid option.");
			if (option == 1 && one) {
				System.out.println("You've already chosen option 1. "
						+ "\nPlease choose a valid option.");
				reader.nextLine();
				option = 0;
				continue;
			}
			if (option == 2 && two) {
				System.out.println("You've already chosen option 2. "
						+ "\nPlease choose a valid option.");
				reader.nextLine();
				option = 0;
				continue;
			}
			if (option == 3 && three) {
				System.out.println("You've already chosen option 3. "
						+ "\nPlease choose a valid option.");
				reader.nextLine();
				option = 0;
				continue;
			}
			if (option == 4 && four) {
				System.out.println("You've already chosen option 4. "
						+ "\nPlease choose a valid option.");
				reader.nextLine();
				option = 0;
				continue;
			}
			if (option == 1) one = true;
			if (option == 2) two = true;
			if (option == 3) three = true;
			if (option == 4) four = true;
		}
		System.out.println("Thanks for choosing.\n");
		
		FileReader aFile = null;
		try {
			aFile = new FileReader("sidewalk.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scanner inFile = new Scanner(aFile);
		@SuppressWarnings("unused")
		String line = "";
		int count = 0;
		while (!inFile.hasNext("xxx")) {
			line = inFile.nextLine();
			count++;
		}
		System.out.println(count + " lines read.");
		
		String theInput = new String("15784,15784,"
				+ "Harlem/Lake Green Line Station,"
				+ "41.88675931,-87.80319483,"
				+ "0,,1");
		String stringArray [] = theInput.split(",");
		System.out.println("Length of string array: " + 
		stringArray.length + "\n");
		for (int i = 0; i < stringArray.length; i++) {
			System.out.println(stringArray[i]);
		}
		
		//String split() treated the "empty" commas as 
		//as a space
		
		reader.close();
		inFile.close();
		
	}

}
