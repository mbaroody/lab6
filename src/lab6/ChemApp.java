package lab6;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ChemApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FileReader aFile1 = null;
		try {
			aFile1 = new FileReader("toxicChems.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner inFile1 = new Scanner(aFile1);
		
		
		StringArr input = new StringArr(98);
		while (inFile1.hasNext()) {
			input.add(inFile1.nextLine());
		}
		
		System.out.println(input.toString());
		FileReader aFile2 = null;
		try {
			aFile2 = new FileReader("toxicChems.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Scanner inFile2 = new Scanner(aFile2);
		StringArr uses = new StringArr(98);
		String line = "";
		while (inFile2.hasNext()) {
			line = inFile2.nextLine();
			uses.add(line.subSequence(
					line.lastIndexOf(',') + 1, line.length()).toString());
		}
		
		System.out.println(uses.toString());
		inFile1.close();
		inFile2.close();
		

	}

}
